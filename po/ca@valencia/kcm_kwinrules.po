# Translation of kcm_kwinrules.po to Catalan (Valencian)
# Copyright (C) 2004-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Sebastià Pla i Sanz <sps@sastia.com>, 2004, 2005, 2006.
# Albert Astals Cid <aacid@kde.org>, 2005.
# Josep M. Ferrer <txemaq@gmail.com>, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2012, 2013, 2014, 2015, 2017, 2020.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-13 02:16+0000\n"
"PO-Revision-Date: 2023-04-13 11:49+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr "Còpia de %1"

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "Valors d'aplicació per a %1"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "Valors de la finestra per a %1"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "Sense importància"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "Coincidència exacta"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "Coincidència parcial"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "Expressió regular"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "Aplica inicialment"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""
"La propietat de la finestra només s'establirà al valor indicat després de "
"crear la finestra.\n"
"Els canvis posteriors no es veuran afectats."

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "Aplica ara"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""
"La propietat de la finestra s'establirà immediatament al valor indicat i no "
"es veurà afectat més tard\n"
"(esta acció se suprimirà més tard)."

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "Recorda"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""
"Es recordarà el valor de la propietat de la finestra, i s'aplicarà l'últim "
"valor recordat cada vegada que es cree la finestra."

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "No afecta"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""
"La propietat de la finestra no es veurà afectada i per tant s'utilitzarà la "
"gestió predeterminada per a ella.\n"
"En especificar açò, es blocaran que tinguen efecte les opcions més "
"genèriques de finestra."

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "Força"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr "La propietat de la finestra sempre es forçarà al valor indicat."

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "Força temporalment"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""
"La propietat de la finestra es forçarà al valor indicat fins que s'oculte\n"
"(esta acció se suprimirà després d'ocultar la finestra)."

#: package/contents/ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr "Seleccioneu un fitxer"

#: package/contents/ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "Regles de KWin (*.kwinrule)"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr "Actualment no s'han definit regles per a finestres específiques"

#: package/contents/ui/main.qml:61
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New...</interface> button below to add some"
msgstr ""
"Feu clic en el botó <interface>Afig nova...</interface> de davall per a "
"afegir-ne"

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Select the rules to export"
msgstr "Seleccioneu les regles que s'han d'exportar"

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Unselect All"
msgstr "Desselecciona-ho tot"

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Select All"
msgstr "Selecciona-ho tot"

#: package/contents/ui/main.qml:87
#, kde-format
msgid "Save Rules"
msgstr "Guarda les regles"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Add New..."
msgstr "Afig nova..."

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Import..."
msgstr "Importa..."

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Cancel Export"
msgstr "Cancel·la l'exportació"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Export..."
msgstr "Exporta..."

#: package/contents/ui/main.qml:207
#, kde-format
msgid "Edit"
msgstr "Edita"

#: package/contents/ui/main.qml:216
#, kde-format
msgid "Duplicate"
msgstr "Duplica"

#: package/contents/ui/main.qml:225
#, kde-format
msgid "Delete"
msgstr "Suprimix"

#: package/contents/ui/main.qml:238
#, kde-format
msgid "Import Rules"
msgstr "Importa algunes regles"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Export Rules"
msgstr "Exporta les regles"

#: package/contents/ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr "Res seleccionat"

#: package/contents/ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr "Tot seleccionat"

#: package/contents/ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] "%1 seleccionat"
msgstr[1] "%1 seleccionats"

#: package/contents/ui/RulesEditor.qml:63
#, kde-format
msgid "No window properties changed"
msgstr "No han canviat les propietats de les finestres"

#: package/contents/ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""
"Feu clic en el botó <interface>Afig propietat...</interface> de davall per a "
"afegir diverses propietats de les finestres que seran afectades per la regla"

#: package/contents/ui/RulesEditor.qml:85
#, kde-format
msgid "Close"
msgstr "Tanca"

#: package/contents/ui/RulesEditor.qml:85
#, kde-format
msgid "Add Property..."
msgstr "Afig una propietat..."

#: package/contents/ui/RulesEditor.qml:98
#, kde-format
msgid "Detect Window Properties"
msgstr "Detecta les propietats de la finestra"

#: package/contents/ui/RulesEditor.qml:114
#: package/contents/ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr "Instantàniament"

#: package/contents/ui/RulesEditor.qml:115
#: package/contents/ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "Després d'%1 segon"
msgstr[1] "Després de %1 segons"

#: package/contents/ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr "Afig una propietat a la regla"

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "Sí"

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "No"

#: package/contents/ui/RulesEditor.qml:278
#: package/contents/ui/ValueEditor.qml:168
#: package/contents/ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr "%1 %"

#: package/contents/ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: package/contents/ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: package/contents/ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr "x"

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "Valors per a %1"

#: rulesmodel.cpp:221
#, kde-format
msgid "New window settings"
msgstr "Configureu les finestres noves"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"Heu especificat la classe de finestra com a sense importància.\n"
"Açò significa que els valors possiblement aplicaran a les finestres de totes "
"les aplicacions. Si segur que voleu crear un configuració genèric, es "
"recomana que, com a mínim, limiteu els tipus de finestra per tal d'evitar "
"tipus de finestra especials."

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""
"Diverses aplicacions definixen la seua pròpia geometria després d'iniciar-"
"se, substituint la vostra configuració inicial de mida i posició. Per a "
"forçar esta configuració, forceu també la propietat «%1» a «Sí»."

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""
"La llegibilitat es pot veure afectada amb valors d'opacitat extremadament "
"baixos. Al 0%, la finestra es torna invisible."

#: rulesmodel.cpp:382
#, kde-format
msgid "Description"
msgstr "Descripció"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, kde-format
msgid "Window matching"
msgstr "Coincidència de la finestra"

#: rulesmodel.cpp:390
#, kde-format
msgid "Window class (application)"
msgstr "Classe de finestra (aplicació)"

#: rulesmodel.cpp:398
#, kde-format
msgid "Match whole window class"
msgstr "Coincidix amb la classe de finestra completa"

#: rulesmodel.cpp:405
#, kde-format
msgid "Whole window class"
msgstr "Classe de finestra completa"

#: rulesmodel.cpp:411
#, kde-format
msgid "Window types"
msgstr "Tipus de finestra"

#: rulesmodel.cpp:419
#, kde-format
msgid "Window role"
msgstr "Funció de la finestra"

#: rulesmodel.cpp:424
#, kde-format
msgid "Window title"
msgstr "Títol de la finestra"

#: rulesmodel.cpp:430
#, kde-format
msgid "Machine (hostname)"
msgstr "Màquina (nom de la màquina)"

#: rulesmodel.cpp:436
#, kde-format
msgid "Position"
msgstr "Posició"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, kde-format
msgid "Size & Position"
msgstr "Mida i posició"

#: rulesmodel.cpp:442
#, kde-format
msgid "Size"
msgstr "Mida"

#: rulesmodel.cpp:448
#, kde-format
msgid "Maximized horizontally"
msgstr "Maximitzada horitzontalment"

#: rulesmodel.cpp:453
#, kde-format
msgid "Maximized vertically"
msgstr "Maximitzada verticalment"

#: rulesmodel.cpp:461
#, kde-format
msgid "Virtual Desktop"
msgstr "Escriptori virtual"

#: rulesmodel.cpp:467
#, kde-format
msgid "Virtual Desktops"
msgstr "Escriptoris virtuals"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr "Activitats"

#: rulesmodel.cpp:502
#, kde-format
msgid "Screen"
msgstr "Pantalla"

#: rulesmodel.cpp:507
#, kde-format
msgid "Fullscreen"
msgstr "Pantalla completa"

#: rulesmodel.cpp:512
#, kde-format
msgid "Minimized"
msgstr "Minimitzada"

#: rulesmodel.cpp:517
#, kde-format
msgid "Shaded"
msgstr "Plegada"

#: rulesmodel.cpp:522
#, kde-format
msgid "Initial placement"
msgstr "Emplaçament inicial"

#: rulesmodel.cpp:531
#, kde-format
msgid "Ignore requested geometry"
msgstr "Ignora la geometria demanada"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""
"Algunes aplicacions poden establir la seua pròpia geometria, substituint les "
"preferències del gestor de finestres. Establir esta propietat substituïx les "
"seues sol·licituds de col·locació.<nl/><nl/>Açò afecta la <interface>Mida</"
"interface> i la <interface>Posició</interface> però no els estats de "
"<interface>Maximització</interface> o <interface>Pantalla completa</"
"interface>.<nl/><nl/>Cal tindre en compte que la posició també es pot emprar "
"per a assignar a una <interface>Pantalla</interface> diferent"

#: rulesmodel.cpp:546
#, kde-format
msgid "Minimum Size"
msgstr "Mida mínima"

#: rulesmodel.cpp:551
#, kde-format
msgid "Maximum Size"
msgstr "Mida màxima"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr "Obeïx les restriccions de geometria"

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""
"Algunes aplicacions com els reproductors de vídeo o els terminals poden "
"sol·licitar a KWin que es restringisquen a determinades relacions d'aspecte, "
"o créixer només per a les dimensions d'un caràcter. Utilitzeu esta propietat "
"per a ignorar estes restriccions i permetre que aquelles finestres es "
"redimensionen a mides arbitràries.<nl/><nl/>Açò pot ser útil per a les "
"finestres que no es poden ajustar prou bé a l'àrea de la pantalla completa "
"quan estant maximitzades."

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr "Mantín damunt les altres finestres"

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr "Organització i accés"

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr "Mantín davall les altres finestres"

#: rulesmodel.cpp:579
#, kde-format
msgid "Skip taskbar"
msgstr "Ignora la barra de tasques"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr "Controla si la finestra apareix o no al Gestor de tasques."

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip pager"
msgstr "Ignora el paginador"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr "Controla si la finestra apareix o no al gestor d'escriptoris virtuals."

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip switcher"
msgstr "Ignora el commutador"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""
"Controla si la finestra apareix o no a la llista de finestres <shortcut>Alt"
"+Tab</shortcut>."

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "Drecera"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr "Sense barra de títol ni marc"

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr "Aparença i esmenes"

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr "Esquema de color de la barra de títol"

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr "Opacitat activa"

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr "Opacitat inactiva"

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr "Prevenció de robatori de focus"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""
"KWin intenta evitar que les finestres que s'han obert sense acció directa de "
"l'usuari s'eleven i prenguen el focus mentre esteu interactuant amb una "
"altra finestra. Esta propietat es pot utilitzar per a canviar el nivell de "
"prevenció de robatori de focus aplicat a finestres i aplicacions individuals."
"<nl/><nl/>Açò és el que passarà a una finestra oberta sense la vostra acció "
"directa a cada nivell de prevenció de robatori de focus:<nl/"
"><list><item><emphasis strong='true'>Sense:</emphasis> La finestra s'elevarà "
"i prendrà el focus.</item><item><emphasis strong='true'>Baixa:</emphasis> "
"S'aplicarà la política de prevenció de robatori de focus, però en cas d'una "
"situació que KWin considere ambigua, la finestra s'elevarà i prendrà el "
"focus.</item><item><emphasis strong='true'>Normal:</emphasis> S'aplicarà la "
"política de prevenció de robatori de focus, però en cas d'una situació que "
"KWin considere ambigua, la finestra <emphasis>no</emphasis> s'elevarà ni "
"prendrà el focus. </item><item><emphasis strong='true'>Alta:</emphasis> La "
"finestra només s'elevarà i prendrà el focus si pertany a la mateixa "
"aplicació que la finestra actualment amb focus.</item><item><emphasis "
"strong='true'>Extrema:</emphasis> La finestra mai s'elevarà ni prendrà el "
"focus.</item></list>"

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr "Protecció del focus"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""
"Esta propietat controla el nivell de protecció del focus de la finestra "
"actualment activa. S'utilitza per substituir la prevenció de robatori de "
"focus aplicada a les finestres noves que s'òbriguen sense la vostra acció "
"directa.<nl/><nl/>Açò és el que passa a les finestres noves que s'òbriguen "
"sense la vostra acció directa a cada nivell de protecció de focus mentre la "
"finestra amb esta propietat té el focus:<nl/><list><item><emphasis "
"strong='true'>Sense</emphasis>: Les finestres obertes recentment sempre "
"s'eleven i prenen el focus.</item><item><emphasis strong='true'>Baixa:</"
"emphasis> S'aplicarà la prevenció de robatori de focus a les finestres "
"obertes recentment, però en el cas d'una situació que KWin considere "
"ambigua, la finestra s'elevarà i prendrà el focus.</item><item><emphasis "
"strong='true'>Normal:</emphasis> S'aplicarà la prevenció de robatori de "
"focus a les finestres obertes recentment, però en el cas d'una situació que "
"KWin considere ambigua, la finestra <emphasis>no</emphasis> s'elevarà ni "
"prendrà el focus.</item><item><emphasis strong='true'>Alta:</emphasis> Les "
"finestres obertes recentment només s'elevaran i prendran el focus si "
"pertanyen a la mateixa aplicació que la finestra actualment amb focus.</"
"item><item><emphasis strong='true'>Extrema:</emphasis> Les finestres obertes "
"recentment mai s'elevaran ni prendran el focus.</item></list>"

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr "Accepta el focus"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr "Controla si la finestra prendrà el focus o no quan s'hi fa clic."

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr "Ignora les dreceres globals"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""
"Utilitzeu esta propietat per a evitar que les dreceres de teclat globals "
"funcionen mentre la finestra tinga el focus. Açò pot ser útil per a "
"aplicacions com emuladors o màquines virtuals que gestionen algunes de les "
"mateixes dreceres.<nl/><nl/>Cal tindre en compte que no podreu fer "
"<shortcut>Alt+Tab</shortcut> fora de la finestra o utilitzar altres dreceres "
"globals com <shortcut>Alt+Espai</shortcut> per a activar KRunner."

#: rulesmodel.cpp:698
#, kde-format
msgid "Closeable"
msgstr "Es pot tancar"

#: rulesmodel.cpp:703
#, kde-format
msgid "Set window type"
msgstr "Establix el tipus de finestra"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr "Nom de fitxer «desktop»"

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "Bloqueja la composició"

#: rulesmodel.cpp:766
#, kde-format
msgid "Window class not available"
msgstr "Classe de finestra no disponible"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""
"Esta aplicació no proporciona cap classe per a la finestra, així que KWin no "
"la pot utilitzar per a fer-la coincidir i aplicar les regles. Si encara "
"voleu aplicar-hi regles, intenteu fer-hi coincidir altres propietats com el "
"títol de la finestra.<nl/><nl/>Considereu informar d'este error als "
"desenvolupadors de l'aplicació."

#: rulesmodel.cpp:801
#, kde-format
msgid "All Window Types"
msgstr "Tots els tipus de finestra"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "Finestra normal"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "Finestra de diàleg"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "Finestra d'utilitat"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "Acoblador (quadro)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "Barra d'eines"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "Menú separable"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "Pantalla de presentació"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "Escriptori"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "Barra de menús aïllada"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr "Visualització en pantalla"

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "Tots els escriptoris"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr "Fa que la finestra estiga disponible en tots els escriptoris"

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr "Totes les activitats"

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr "Fa que la finestra estiga disponible en totes les activitats"

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "Predeterminat"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "Sense emplaçament"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr "Superposició mínima"

#: rulesmodel.cpp:869
#, kde-format
msgid "Maximized"
msgstr "Maximitzada"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "Centrada"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "Aleatòria"

#: rulesmodel.cpp:872
#, kde-format
msgid "In Top-Left Corner"
msgstr "En el cantó superior esquerre"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "Davall el ratolí"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "En la finestra principal"

#: rulesmodel.cpp:881
#, kde-format
msgid "None"
msgstr "Sense"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "Baixa"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "Normal"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "Alta"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "Extrema"

#: rulesmodel.cpp:928
#, kde-format
msgid "Unmanaged window"
msgstr "Finestra no gestionada"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""
"No s'han pogut detectar les propietats de la finestra. KWin no gestiona esta "
"finestra."
